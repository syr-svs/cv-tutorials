from matplotlib import pyplot as plt
import cv2

face_cascade = cv2.CascadeClassifier('data/cascades/haarcascades/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('data/cascades/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

# VideoCapture device: Your webcam
cap = cv2.VideoCapture(0)

try:
    while True:

        # reads frames from a camera
        ret, img = cap.read()

        # convert to gray scale of each frames
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Detects faces of different sizes in the input image
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        for (x,y,w,h) in faces:
            # To draw a rectangle in a face
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,255,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]

            # Detects eyes of different sizes in the input image
            eyes = eye_cascade.detectMultiScale(roi_gray)

            #To draw a rectangle in eyes
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,127,255),2)

        # Display an image in a window
        try:
            _ = im.set_data(img[..., ::-1])
        except:
            im = plt.imshow(img[..., ::-1])
        plt.pause(.01)
        plt.draw()

except Exception as ex:
    print(ex)
finally:

    # Close the window
    cap.release()

    # De-allocate any associated memory usage
    cv2.destroyAllWindows()